Whether you're looking to make a purchase or rent equipment, we have you covered. Our Service Department can handle preventive maintenance, axle replacements, body work and much more. For more information call (336) 475-5054!

Address: 221 Boots Evans Road, Thomasville, NC 27360, USA

Phone: 336-475-5054